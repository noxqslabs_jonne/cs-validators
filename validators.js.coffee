class window.ValidationError extends Error
  constructor: (validator)->
    super()

    @message = "Validation failed for #{validator}"

window.validators =
  range: (opt)->
    {min, max, strict} = opt
    strict = true if strict == undefined

    (value)->
      if !(strict in ['false', '0', 'no'])
        for v in [min, max, value]
          return false if (isNaN(parseFloat(v)) || !isFinite(v));

      return (Number(min) <= Number(value) <= Number(max))

  regex: (opt)->
    {pattern} = opt
    (value)-> (value.match(new RegExp(pattern)) instanceof Array)

applyValidators = (el)->
  validators = $(el).attr('data-validators')
  return false if not validators

  $(el).data('validators', {})

  validators = validators.split(',')

  $.map validators, (validator)->
    opt = validatorOpts(el, validator)

    $(el).data('validators')[validator] = window.validators[validator](opt)

validatorOpts = (el, validator)->
  opt = {}

  $.each $(el)[0].attributes, (k,attr)->
    if match = attr.name.match ///data-validate-#{validator}-(.*)///
      [match, attrName] = match
      opt[attrName] = attr.value

  return opt

validate = (el)->
  return false if !(validators = $(el).data('validators'))

  $.each validators, (k,v)->
    if !v($(el).val())
      throw new ValidationError(k)

$.fn.validate = ->
  validate(this)

$ ->
  $('[data-validators]').each (k,el)->
    applyValidators(el)

  validateForm = (form)->
    errors = []
    $(form).find('[data-validators]').each (k,el)->
      try
        $(el).validate()
      catch e
        throw(e) if not e instanceof ValidationError
        errors.push e

    $('#errors').empty()
      .append ($.map errors, (error)->
        $('<span class="error">').text(error.message))

  $('#validation-test').on
    submit: (e)-> e.preventDefault(); validateForm(e.target)

  `var fn`
  fn = -> validateForm($('#validation-test'))

  $('#validation-test').find('[data-validators]').on
    keydown: fn
    change: fn
